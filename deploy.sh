#!/bin/bash
# We're assuming that kubernetes cluster is created and kubectl is communicating with its API server

git clone https://gitlab.com/czeslavo/vidog-deployment.git
cd vidog-deployment

# Create temporary directory for binaries
BIN=$(mktemp -d)
function removeTempAndRepo {
    echo "Cleaning up..."
    echo "Removing temporary BIN dir..." && rm -rf ${BIN}
    echo "Removing repository..." && cd .. && rm -rf vidog-deployment
}
trap removeTempAndRepo EXIT
export PATH=$BIN:$PATH


# Install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl ${BIN}/

# Install k3d
curl -LO https://github.com/rancher/k3d/releases/download/v1.3.4/k3d-linux-amd64
chmod +x ./k3d-linux-amd64
mv ./k3d-linux-amd64 ${BIN}/k3d

# Checkout to a tag which is used in the thesis
git checkout tags/v0.1.6

# Create cluster in k3d
k3d create --publish 8080:30080@k3d-vidog-cluster-worker-0 --publish 1935:30935@k3d-vidog-cluster-worker-0 --name vidog-cluster --workers 2
echo "Waiting for cluster to become ready..." && sleep 10

function removeCluster {
    echo "Removing cluster..." && k3d delete --name vidog-cluster 
}
function cleanup {
    removeCluster
    removeTempAndRepo
}
trap cleanup EXIT

# Export configuration for kubectl
export KUBECONFIG="$(k3d get-kubeconfig --name='vidog-cluster')"


# Create Stream CRD in cluster
kubectl apply -f ./cluster_wide/stream_crd.yaml

# Create vidog-prod namespace
kubectl create namespace vidog-prod

# Deploy all manifests meant to be deployed to prod
kubectl apply -R -f ./prod/

until kubectl get pods -n vidog-prod | grep stream-receiver | grep Running
do
    echo "Waiting for stream-receiver..." && sleep 1
done

until kubectl get pods -n vidog-prod | grep web-ui | grep Running
do
    echo "Waiting for web-ui..." && sleep 1
done

echo
echo "To access UI, please visit http://localhost:8080"
echo "You can stream video to rtmp://localhost:1935/stream/{yourStreamName}"
echo

echo "CTRL+C to finish" 
echo "Streaming sample video to stream 'test'. Create stream with this name from UI to watch it."
docker run --network host -i jrottenberg/ffmpeg -stream_loop -1 -i https://test-videos.co.uk/vids/jellyfish/mp4/h264/720/Jellyfish_720_10s_5MB.mp4 -f flv rtmp://localhost:1935/stream/test

